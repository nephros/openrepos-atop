%define upstream_name atop
%define myunitdir %{_unitdir}
%define pmunitdir %{myunitdir}/../system-sleep

Name:		openrepos-atop
Version:	2.6.0
Release:	1
Summary:	an ASCII full-screen performance monitor for Linux
License:    GNU General Public Licence v3 or any later version
URL:		https://www.atoptool.nl/

Source0:    %{upstream_name}/%{upstream_name}-%{version}.tar.gz
Source1:	%{name}.default
Source2:	%{name}rc
 
# see https://github.com/Atoptool/atop/commit/aff473ee28903775e1bb35793b9c4c50ee0c7270#diff-3d5bd9bffb1d9916c3dcc10a01540ea9152efb40df2526a8becf5976a4a92789
Patch0:	%{upstream_name}-ethtool-4.6.patch
# see https://github.com/Atoptool/atop/commit/a4503da46769329318ee3483ae818d8ff4bf5de7#diff-3d5bd9bffb1d9916c3dcc10a01540ea9152efb40df2526a8becf5976a4a92789
#Patch1:	%{upstream_name}-ethtool-4.6-conditional.patch


BuildRequires:	zlib-devel
BuildRequires:	ncurses-devel
Requires:	zlib
Requires:	ncurses-libs
Requires:	(busybox-symlinks-findutils or findutils)

# just in case someone builds an "official" version
Conflicts: atop

BuildRoot: %{_tmppath}/%{upstream_name}-%{version}-%{release}-root

%description
Atop is an ASCII full-screen performance monitor for Linux that is capable of
reporting the activity of all processes (even if processes have finished during
the interval), daily logging of system and process activity for long-term
analysis, highlighting overloaded system resources by using colors, etc. At
regular intervals, it shows system-level activity related to the CPU, memory,
swap, disks (including LVM) and network layers, and for every process (and
thread) it shows e.g. the CPU utilization, memory growth, disk utilization,
priority, username, state, and exit code.

%prep
%autosetup -p1 -n %{upstream_name}-%{version}

%build
sed -i -e 's/atop\.service/openrepos-atop.service/' atopacct.service
sed -i -e 's/atop\.service/openrepos-atop.service/' atopgpu.service
sed -i -e 's/atop\.service/openrepos-atop.service/' atop-rotate.service
sed -i -e 's/atop/openrepos-atop/' atop-pm.sh
gmake %{?_smp_mflags} atop atopacctd atopconvert
ln -sf atop				atopsar
ln -sf atop atop-%{version}
ln -sf atop-%{version}	atopsar-%{version}

%install
%{__rm} -rf %{buildroot}
## the installtargets do al kinds of things manually.
## we prefer the install script
%{__install} -D -p -m 644	%{SOURCE1}	$RPM_BUILD_ROOT%{_sysconfdir}/default/atop
%{__install} -D -p -m 644	%{SOURCE2}	$RPM_BUILD_ROOT%{_sysconfdir}/atoprc
%{__install} -D -p -m 04711	atop			$RPM_BUILD_ROOT%{_bindir}/atop
%{__install} -D -p			atopsar			$RPM_BUILD_ROOT%{_bindir}/atopsar
%{__install} -D -p			atopsar-%{version} $RPM_BUILD_ROOT%{_bindir}/atopsar-%{version}
%{__install} -D -p -m 0700	atopacctd		$RPM_BUILD_ROOT%{_sbindir}/atopacctd
%{__install} -D -p -m 0700	atopgpud		$RPM_BUILD_ROOT%{_sbindir}/atopgpud
%{__install} -D -p -m 0711	atopconvert		$RPM_BUILD_ROOT%{_bindir}/atopconvert
# systemd files
for f in atop-rotate.service atop-rotate.timer atop.service atopacct.service atopgpu.service
do
  %{__install} -D -p -m  0644 ${f} $RPM_BUILD_ROOT%{myunitdir}/openrepos-${f}
done
%{__install} -D -p -m 0711	atop-pm.sh		$RPM_BUILD_ROOT%{pmunitdir}/openrepos-atop-pm.sh
# log dir, otherwise the service will not start
install -d $RPM_BUILD_ROOT%{_localstatedir}/log/atop

%files
%defattr(-,root,root,-)
%license COPYING
%{_bindir}
%{_sbindir}
%{_libdir}
%{_sysconfdir}
%{_localstatedir}

%post
systemctl --no-reload disable openrepos-atop >/dev/null 2>&1 || :
systemctl --no-reload disable openrepos-atopacct >/dev/null 2>&1 || :
systemctl daemon-reload >/dev/null 2>&1 || :
#systemctl enable openrepos-atopacct >/dev/null 2>&1 || :
#systemctl enable openrepos-atop >/dev/null 2>&1 || :
#systemctl enable openrepos-atop-rotate.timer >/dev/null 2>&1 || :


%preun
#atop-rotate.service atop-rotate.timer atop.service atopacct.service atopgpu.service
systemctl --no-reload disable openrepos-atopacct.service >/dev/null 2>&1 || :
systemctl stop  openrepos-atopacct.service >/dev/null 2>&1 || :
systemctl --no-reload disable openrepos-atop.service >/dev/null 2>&1 || :
systemctl stop  openrepos-atop.service >/dev/null 2>&1 || :
systemctl --no-reload disable openrepos-atopgpu.service >/dev/null 2>&1 || :
systemctl stop  openrepos-atopgpu.service >/dev/null 2>&1 || :
systemctl --no-reload disable openrepos-atop-rotate.service >/dev/null 2>&1 || :
systemctl stop  openrepos-atop-rotate.service >/dev/null 2>&1 || :
systemctl --no-reload disable openrepos-atop-rotate.timer >/dev/null 2>&1 || :
systemctl stop  openrepos-atop-rotate.timer >/dev/null 2>&1 || :

%postun
rm %{_localstatedir}/log/atop/atop_*
systemctl daemon-reload >/dev/null 2>&1 ||:

%changelog
* Thu Sep  3 10:39:13 CEST 2020 Nephros <sailfish@nephros.org> 2.5.0-7
- disable activating/enabling systemd services, ot prevent bad things happening
* Thu Sep  3 10:39:13 CEST 2020 Nephros <sailfish@nephros.org> 2.5.0-6
- first Release for SFOS/OpenRepos
* Wed Sep  2 17:11:47 CEST 2020 Nephros <sailfish@nephros.org> 2.5.0-1
- initial version of spec file


# this is a non-ASCII character to shut up rpmlint: «
# vim: fileencoding=utf-8


